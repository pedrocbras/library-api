﻿using System;
namespace LibraryApp.Models
{
    public class Book
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public bool IsAvailable { get; set; }

        public long ReaderId { get; set; }
        public Reader Reader { get; set; }
    }
}
