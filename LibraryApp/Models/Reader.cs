﻿using System;
using System.Collections.Generic;
namespace LibraryApp.Models
{
    public class Reader
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public List<Book> Books { get; set; }
    }
}
