﻿using Microsoft.EntityFrameworkCore;

namespace LibraryApp.Models
{
    public class LibraryContext : DbContext
    {

        public DbSet<Book> Books { get; set; }
        public DbSet<Reader> Readers { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>()
                .HasOne(b => b.Reader)
                .WithMany(r => r.Books)
                .HasForeignKey(b => b.ReaderId);
        }

        public LibraryContext(DbContextOptions<LibraryContext> options)
            : base(options)
        {
        }
    }
}
